<?php

namespace App\Http\Controllers;
use App\model\Acount;
use Illuminate\Http\Request;

class AcountController extends Controller
{
    public function index(){
        return Acount::all();
    }
    public function store(Request $request){
        $client = new Acount();
        $client->fill($request->toArray())->save();
		return $client;
    }
	public function destroy($id){
        $client=Acount::find($id);
        $client->delete();
    }

    public function show($id){
        $client=Acount::find($id);
        return $client;
    }
    public function update($id,Request $request){
        $client=$this->show($id);
        $client->fill($request->all())->save();
        return $client;
    }
}
