<?php

namespace App\Http\Controllers;
use App\model\DetalOrder;
use Illuminate\Http\Request;

class DetalOrderController extends Controller
{
    public function index(){
        return DetalOrder::all();
    }
    public function store(Request $request){
        $client = new DetalOrder();
        $client->fill($request->toArray())->save();
		return $client;
    }
    public function destroy($id){
        $client=DetalOrder::find($id);
        $client->delete();
    }
    public function show($id){
        $client=DetalOrder::find($id);
        return $client;
    }
    public function update($id,Request $request){
        $client=$this->show($id);
        $client->fill($request->all())->save();
        return $client;
    }

}
