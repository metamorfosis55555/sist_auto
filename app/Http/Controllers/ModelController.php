<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\model\Models;
class ModelController extends Controller
{
    public function index(){
        return Models::all();
    }
    public function store(Request $request){
        $client = new Models();
        $client->fill($request->toArray())->save();
		return $client;
    }
    public function destroy($id){
        $client=Models::find($id);
        $client->delete();
    }
    public function show($id){
        $client=Models::find($id);
        return $client;
    }
    public function update($id,Request $request){
        $client=$this->show($id);
        $client->fill($request->all())->save();
        return $client;
    }
    
}
