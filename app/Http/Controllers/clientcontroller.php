<?php

namespace App\Http\Controllers;
use App\model\client;
use Illuminate\Http\Request;

class clientcontroller extends Controller
{
    public function index(){
        return client::all();
    }
    public function store(Request $request){
        $client = new client();
        $client->fill($request->toArray())->save();
		return $client;
    }
    public function destroy($id){
        $client=client::find($id);
        $client->delete();
    }
    public function show($id){
        $client=client::find($id);
        return $client;
    }
    public function update($id,Request $request){
        $client=$this->show($id);
        $client->fill($request->all())->save();
        return $client;
    }
}
