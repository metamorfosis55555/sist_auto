<?php

namespace App\Http\Middleware;

use Closure;

class detalorder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if((int)$request->header('detalorder')===3464){
            return$next($request);
        }
        return response('Error',403);
    }
}
