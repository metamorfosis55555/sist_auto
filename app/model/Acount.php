<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model;

class Acount extends Model
{
    protected $table='acount';
    protected $primaryKey="id_acount";
    public $timestamps=true;
    const created_at = 'created_ad';
    const update_at = 'updated_ad';
    protected $fillable =[
        'name',
        'password',
        'created_ad',
        'updated_ad'
    ];
}
