<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $table='vehicle';
    protected $primaryKey="id_vehicle";
    public $timestamps=true;
    const created_at = 'create_ad';
    const update_at = 'update_ad';
    protected $fillable =[
        'enrollent',
        'model',
        'colour',
		'brand',
		'mileage',
		'year',
        'created_ad',
        'updated_ad'
    ];
}
