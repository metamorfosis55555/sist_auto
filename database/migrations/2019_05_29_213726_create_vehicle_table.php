<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle', function (Blueprint $table) {
            $table->bigIncrements('id_vehicle');
			$table->string('enrollent')->nullable();//matricula
			$table->string('model')->nullable();//id_model
			$table->string('colour')->nullable();//colr
			$table->string('brand')->nullable();//marca
			$table->bigInteger('mileage')->nullable();//kilometraje
			$table->bigInteger('year')->nullable();//ano
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle');
    }
}
