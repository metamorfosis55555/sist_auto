<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware(['authAcount'])->group(function(){
Route::resource('/Acount','AcountController');
});
Route::middleware(['Mclient'])->group(function(){
    Route::resource('/client','clientcontroller');
});
Route::middleware(['Mcost'])->group(function(){
   Route::resource('/Cost','CostController');
});
Route::middleware(['Mdetalorder'])->group(function(){
    Route::resource('/DetalOrder','DetalOrderController');
 });
 Route::middleware(['Mmodels'])->group(function(){
    Route::resource('/Models','ModelController');
 });
 Route::middleware(['Morder'])->group(function(){
    Route::resource('/Order','OrderController');
 });
 Route::middleware(['Muser'])->group(function(){
    Route::resource('/Users','UsersController');
 });
 Route::middleware(['Mvehicle'])->group(function(){
    Route::resource('/Vehicle','VehicleController');
 });
//Route:: get('Acount/{id}', 'AcountController@get');
//Route:: post('Acount/{id}', 'AcountController@update');
