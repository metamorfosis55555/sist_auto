<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   return view('welcome');
});
Route::resource('/client','clientcontroller');
Route::resource('/Cost','CostController');
Route::resource('/DetalOrder','DetalOrderController');
Route::resource('/Models','ModelController');
Route::resource('/Acount','AcountController');
Route::resource('/Order','OrderController');
Route::resource('/Users','UsersController');
Route::resource('/Vehicle','VehicleController');

Auth::routes();
Route::get('/home','homeController@index')->name('home');
