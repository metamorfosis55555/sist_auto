﻿# Host: localhost  (Version 5.5.5-10.1.34-MariaDB)
# Date: 2019-07-04 20:18:38
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "acount"
#

DROP TABLE IF EXISTS `acount`;
CREATE TABLE `acount` (
  `id_acount` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_acount`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "acount"
#

INSERT INTO `acount` VALUES (1,'belinda','2525','2019-06-04 21:19:45','2019-06-04 21:22:31'),(3,'pedro','2522','2019-06-04 17:39:57','2019-06-04 17:39:57');

#
# Structure for table "client"
#

DROP TABLE IF EXISTS `client`;
CREATE TABLE `client` (
  `id_client` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `CI` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_client`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "client"
#

INSERT INTO `client` VALUES (1,21445,'pepito','paredes','calle sucre','sacaba','edgar@mail.com',7485,'2019-06-04 21:17:29','2019-06-04 17:41:27'),(2,21445,'brenda','paredes','calle sucre','sacaba','edgar@mail.com',7485,'2019-06-04 21:17:34','2019-06-04 21:17:34'),(3,21445,'brenda','paredes vulagrio','calle sucre','sacaba','edgar@mail.com',7485,'2019-06-04 21:17:34','2019-06-04 21:27:17'),(4,21445,'brenda','paredes','calle sucre','sacaba','edgar@mail.com',7485,'2019-06-04 21:17:36','2019-06-04 21:17:36');

#
# Structure for table "cost"
#

DROP TABLE IF EXISTS `cost`;
CREATE TABLE `cost` (
  `id_cost` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_cost` bigint(20) DEFAULT NULL,
  `discount` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_cost`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "cost"
#

INSERT INTO `cost` VALUES (1,'pedro','cosa',254,25,'2019-06-04 17:41:57','2019-06-04 17:41:57'),(2,'pedro','cosa',254,25,'2019-06-04 17:42:01','2019-06-04 17:42:01');

#
# Structure for table "detalorder"
#

DROP TABLE IF EXISTS `detalorder`;
CREATE TABLE `detalorder` (
  `id_detalorder` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `price_per_car` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `return_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `days` bigint(20) DEFAULT NULL,
  `namber_orders` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_detalorder`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "detalorder"
#

INSERT INTO `detalorder` VALUES (1,'255','ced','sdasda','1sdsadsa',25,25,'2019-06-04 17:42:06','2019-06-04 17:42:06'),(2,'255','ced','sdasda','1sdsadsa',25,25,'2019-06-04 17:42:09','2019-06-04 17:42:09');

#
# Structure for table "migrations"
#

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "migrations"
#

INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_05_29_212459_create_client_table',1),(4,'2019_05_29_213609_create_order_table',1),(5,'2019_05_29_213640_create_detalorder_table',1),(6,'2019_05_29_213702_create_cost_table',1),(7,'2019_05_29_213726_create_vehicle_table',1),(8,'2019_05_29_213740_create_model_table',1),(9,'2019_05_29_213815_create_user_table',1),(10,'2019_05_29_213844_create_acount_table',1);

#
# Structure for table "model"
#

DROP TABLE IF EXISTS `model`;
CREATE TABLE `model` (
  `id_model` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name_model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `namber_of_seats` bigint(20) DEFAULT NULL,
  `manufacturing_date` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_model`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "model"
#

INSERT INTO `model` VALUES (1,'toyota',98,2000,'2019-06-04 17:41:49','2019-07-03 15:14:22'),(2,'foton',98,2000,'2019-06-04 17:41:50','2019-06-04 17:41:50');

#
# Structure for table "order"
#

DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `id_order` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name_client` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_price` bigint(20) DEFAULT NULL,
  `detail_order` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "order"
#

INSERT INTO `order` VALUES (1,'juan',2586,'gsagdgas','dsadasd','2019-06-04 17:42:15','2019-06-04 17:42:15'),(2,'juan',2586,'gsagdgas','dsadasd','2019-06-04 17:42:18','2019-06-04 17:42:18');

#
# Structure for table "password_resets"
#

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "password_resets"
#


#
# Structure for table "user"
#

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "user"
#

INSERT INTO `user` VALUES (1,'edgar45','vicente2544','edad@gmail.com','2019-06-04 17:42:26','2019-06-04 17:42:26'),(2,'edgar45','vicente2544','edad@gmail.com','2019-06-04 17:42:29','2019-06-04 17:42:29');

#
# Structure for table "users"
#

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "users"
#

INSERT INTO `users` VALUES (1,'edgar','edvicente@gmail.com',NULL,'$2y$10$6xTBZiYKB0HOpZWbhVGQdelBUBqW2iEH57tZEh69B.wx7rTWWi0bC',NULL,'2019-07-03 23:35:15','2019-07-03 23:35:15'),(2,'carlitos','carlitos@gmail.com',NULL,'$2y$10$9GK7kxDz.CEMLTR79Zi5YOBJP6Tq/cQ8ZwydJIv8VhFFWxSSjQN3m',NULL,'2019-07-03 23:37:22','2019-07-03 23:37:22');

#
# Structure for table "vehicle"
#

DROP TABLE IF EXISTS `vehicle`;
CREATE TABLE `vehicle` (
  `id_vehicle` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `enrollent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `colour` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mileage` bigint(20) DEFAULT NULL,
  `year` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_vehicle`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "vehicle"
#

INSERT INTO `vehicle` VALUES (1,'gadg','dad','f3rf','frwfrwf',55,548,'2019-06-04 17:42:37','2019-06-04 17:42:37'),(2,'gadg','dad','f3rf','frwfrwf',55,548,'2019-06-04 17:42:40','2019-06-04 17:42:40');
